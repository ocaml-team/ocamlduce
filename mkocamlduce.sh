#!/bin/sh
cd `dirname $0`/build
set -e

# OCamlDuce: don't link ocamlduce.cma by default
export OCAMLDUCE_LIGHTLINK=y
echo "OCAMLDUCE_LIGHTLINK set to $OCAMLDUCE_LIGHTLINK"

if [ -f ../_build/ocamlc ]; then
  BOOT_MODE=n
else
  BOOT_MODE=y
fi

./mkconfig.sh
./mkmyocamlbuild_config.sh
echo "let version_ocamlduce = \"$(head -n1 ../VERSION)\";;" >> ../myocamlbuild_config.ml

${MAKE} -C ../byterun
if [ x"$HAS_NATIVE" = x"y" ]; then
  ${MAKE} -C ../asmrun all meta.o dynlink.o
fi
cd ..

. config/config.sh
. build/targets.sh

OCAMLDUCE_BYTE="otherlibs/ocamlduce/ocamlduce.cma"
OCAMLDUCE_NATIVE="otherlibs/ocamlduce/ocamlduce.cmxa"
OCAMLDUCE_SHARED="otherlibs/ocamlduce/ocamlduce.cmxs"

INI_TARGETS="ocamlc tools/ocamldep.byte"
ALL_TARGETS="$TOPLEVEL $OCAMLDUCE_BYTE $TOOLS_BYTE $OCAMLDOC_BYTE"
if [ x"$HAS_NATIVE" = x"y" ]; then
  INI_TARGETS="$INI_TARGETS $OCAMLOPT_BYTE"
  ALL_TARGETS="$ALL_TARGETS $OCAMLC_NATIVE $OCAMLOPT_NATIVE"
  ALL_TARGETS="$ALL_TARGETS $OCAMLDUCE_NATIVE $TOOLS_NATIVE"
fi

OCAML_STDLIB_DIR="$(ocamlc -where)"
find "${OCAML_STDLIB_DIR}/ocamldoc" -type f | sed "s@^${OCAML_STDLIB_DIR}/@@" > ocamlducedoc.itarget
ALL_TARGETS="$ALL_TARGETS ocamlducedoc.otarget"

set -x

if [ x"$BOOT_MODE" = x"y" ]; then
  ocamlbuild "$@" -just-plugin -byte-plugin
  ocamlbuild "$@" ocamlc
  find _build -name '*.cm*' -exec rm \{\} \;
fi
ocamlbuild "$@" $INI_TARGETS
ocamlbuild "$@" $ALL_TARGETS

rm -f _build/tools/ocamldep
cp -p _build/tools/ocamldep.byte _build/tools/ocamldep
if [ x"$HAS_NATIVE" = x"y" ]; then
  rm -f _build/tools/ocamldep.opt
  cp -p _build/tools/ocamldep.native _build/tools/ocamldep.opt
  ocamlopt -shared -linkall -o "_build/$OCAMLDUCE_SHARED" "_build/$OCAMLDUCE_NATIVE"
fi
